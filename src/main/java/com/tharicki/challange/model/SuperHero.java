package com.tharicki.challange.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Base64;

@Entity
public class SuperHero {

    @Id
    private long id;
    private String name;
    private String description;
    private String picture;

    public SuperHero() {
    }

    public SuperHero(long id, String name, String description, String picture) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.picture = picture;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void update(SuperHero superHero) {
        superHero.setName(this.name);
        superHero.setDescription(this.description);
        superHero.setPicture(this.picture);
    }
}
